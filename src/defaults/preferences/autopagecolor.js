pref("extensions.autopagecolor.enabled",false);
pref("extensions.autopagecolor.firstrun",true);
pref("extensions.autopagecolor.logging",false);
pref("extensions.autopagecolor.alerts",true);
pref("extensions.autopagecolor.latitude","51.482578");
pref("extensions.autopagecolor.longitude","-0.007659");
pref("extensions.autopagecolor.iplookupsvc","https://ipecho.net/plain/");
pref("extensions.autopagecolor.geolookupsvc","http://ip-api.com/json/");