if("undefined" == typeof(AutoPageColor)) {
	var AutoPageColor = {}
}
Components.utils.import("chrome://autopagecolor/content/apccommon.jsm", AutoPageColor);
Components.utils.import("chrome://autopagecolor/content/suncalc.jsm", AutoPageColor);
Components.utils.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");

AutoPageColor.Overlay = {
	APCTBBtn : null,
	chkEnable : null,
	cmdEnable : null,
	cmdConfigure : null,
	mnuConfigure : null,
	init : function(){
		let firstrun = AutoPageColor.Common.prefBranch.getBoolPref("firstrun",false);
		if(firstrun){
			AutoPageColor.Common.prefBranch.setBoolPref("firstrun",false);
			AutoPageColor.Common.installButton(document, "nav-bar", "apc-btn-toolbar");
			AutoPageColor.Common.installButton(document, "addon-bar", "apc-btn-toolbar");
		}
		AutoPageColor.Overlay.APCTBBtn = document.getElementById("apc-btn-toolbar");
		AutoPageColor.Overlay.cmdConfigure = document.getElementById("cmdAPCTBCfg");
		AutoPageColor.Overlay.cmdEnable = document.getElementById("cmdAPCTBAuto");
		AutoPageColor.Common.logoutput = AutoPageColor.Common.prefBranch.getBoolPref("logging",false);
		AutoPageColor.Common.urlIPLookupSvc = AutoPageColor.Common.prefBranch.getCharPref("iplookupsvc");
		AutoPageColor.Common.urlGeoLookupSvc = AutoPageColor.Common.prefBranch.getCharPref("geolookupsvc");
		AutoPageColor.Common.strBundle = Services.strings.createBundle("chrome://autopagecolor/locale/autopagecolor.properties");
		AutoPageColor.Common.alertTitle = AutoPageColor.Common.strBundle.GetStringFromName("notification.title");
		AutoPageColor.Common.darkLabel = AutoPageColor.Common.strBundle.GetStringFromName("notification.dark");
		AutoPageColor.Common.lightLabel = AutoPageColor.Common.strBundle.GetStringFromName("notification.light");
		AutoPageColor.Common.noneLabel = AutoPageColor.Common.strBundle.GetStringFromName("notification.none");
		AutoPageColor.Overlay.cmdConfigure.addEventListener("command",AutoPageColor.Common.onConfigure,false);
		AutoPageColor.Overlay.cmdEnable.addEventListener("command",AutoPageColor.Overlay.onEnable,false);
	    AddonManager.addAddonListener(AutoPageColor.Common.uninstallListener);
	    Services.obs.addObserver(AutoPageColor.Common.uninstallObserver, "quit-application-granted", false);
		AutoPageColor.Overlay.prefListener.register();
		AutoPageColor.Overlay.globalPrefListener.register();
		if(AutoPageColor.Overlay.APCTBBtn != null) {
			AutoPageColor.Overlay.buttonInit();
		}
	},
	toolbarHandler : {
			buttonAbsent : true,
			toolbarBeforeCustomization : function(){
				if(AutoPageColor.Overlay.APCTBBtn){
					AutoPageColor.Overlay.toolbarHandler.buttonAbsent=false;
				}
			},
			toolbarAfterCustomization : function(){
				AutoPageColor.Overlay.APCTBBtn = document.getElementById("apc-btn-toolbar");
				if(AutoPageColor.Overlay.APCTBBtn == null){
					AutoPageColor.Overlay.buttonUnload();
				} else if(AutoPageColor.Overlay.APCTBBtn && AutoPageColor.Overlay.toolbarHandler.buttonAbsent) {
					AutoPageColor.Overlay.buttonInit();
				}
			}		
	},
	/**
	 * Toolbar button press handler, cycles through light,dark and no preference.
	 */
	onToolbarButton : function(){
		let currentScheme = Services.prefs.getIntPref("browser.display.prefers_color_scheme");
		let status = "notification.";
		switch(currentScheme){
			case 0:
			currentScheme++;
			status+="light";
			break;
			case 1:
			currentScheme++;
			status+="dark";
			break;
			case 2:
			currentScheme=0;
			status+="none";
			break;
		}
		Services.prefs.setIntPref("browser.display.prefers_color_scheme",currentScheme,0);
		document.getElementById("cmdAPCTBBtn").setAttribute("scheme",currentScheme);
		document.getElementById("cmdAPCTBBtn").setAttribute("tooltiptext",
			AutoPageColor.Common.strBundle.formatStringFromName(
					"toolbar.button.tooltip",[AutoPageColor.Common.strBundle.GetStringFromName(status)],1));
	},
	/**
	 * Checkbox event handler. 
	 */
	onEnable : function(event){
		AutoPageColor.Common.prefBranch.setBoolPref("enabled",!AutoPageColor.Common.prefBranch.getBoolPref("enabled",false));
	},
	/**
	 * Initialize toolbar button when added.
	 */
	buttonInit: function(){
		AutoPageColor.Overlay.chkEnable = document.getElementById("apc-btn-chk-auto");
		AutoPageColor.Overlay.mnuConfigure = document.getElementById("apc-btn-mnu-cfg");
		AutoPageColor.Overlay.APCTBBtn.setAttribute('oncommand','cmdAPCTBBtn');
		document.getElementById("cmdAPCTBBtn").addEventListener('command',AutoPageColor.Overlay.onToolbarButton);
		AutoPageColor.Overlay.chkEnable.setAttribute('oncommand','cmdAPCTBAuto');
		AutoPageColor.Overlay.mnuConfigure.setAttribute('oncommand','cmdAPCTBCfg');
		AutoPageColor.Common.prefBranch.getBoolPref("enabled") ? AutoPageColor.Overlay.chkEnable.setAttribute("checked", "true"):AutoPageColor.Overlay.chkEnable.removeAttribute("checked");
		let currentScheme = Services.prefs.getIntPref("browser.display.prefers_color_scheme");
		let status = "notification.";
		switch(currentScheme){
			case 0:
			status+="none";
			break;
			case 1:
			status+="light";
			break;
			case 2:
			status+="dark";
			break;
		}
		if(AutoPageColor.Common.prefBranch.getBoolPref("enabled",false)){
			AutoPageColor.Overlay.chkEnable.setAttribute('checked','true');
		} else {
			AutoPageColor.Overlay.chkEnable.removeAttribute('checked');
		}
		document.getElementById("cmdAPCTBBtn").setAttribute("tooltiptext",
			AutoPageColor.Common.strBundle.formatStringFromName(
					"toolbar.button.tooltip",[AutoPageColor.Common.strBundle.GetStringFromName(status)],1));
	},
	/**
	 * Cleanup toolbar button when removed or browser exiting.
	*/
	buttonUnload : function(){
		AutoPageColor.Overlay.toolbarHandler.buttonAbsent=true;
		document.getElementById("cmdAPCTBBtn").removeEventListener('command',AutoPageColor.Overlay.onToolbarButton);
		document.getElementById("cmdAPCTBCfg").removeEventListener('command',AutoPageColor.Common.onConfigure);
		document.getElementById("cmdAPCTBAuto").removeEventListener('command',AutoPageColor.Common.onEnable);
	},
	/**
	 * Preference listener for preferred color scheme setting to enable/disable the timer.
	**/
	globalPrefListener : new AutoPageColor.APCPrefListener("browser.display.",function(branch,name){
		let currentScheme = branch.getIntPref("prefers_color_scheme");
		let status = "notification.";
		if( currentScheme == 0 || currentScheme > 2){
			AutoPageColor.Common.timer.cancel();
			status+="none";
		}  else {
			AutoPageColor.Common.timer.init(AutoPageColor.Common.timeObserver,
				AutoPageColor.APC_TIMER_DELAY,AutoPageColor.Common.timer.TYPE_REPEATING_PRECISE_CAN_SKIP);
				status+= (currentScheme == 1 ? "light":"dark");
		}
		//change toolbar button to reflect current status.
		document.getElementById("cmdAPCTBBtn").setAttribute("scheme",currentScheme);
		document.getElementById("cmdAPCTBBtn").setAttribute("tooltiptext",
			AutoPageColor.Common.strBundle.formatStringFromName(
					"toolbar.button.tooltip",[AutoPageColor.Common.strBundle.GetStringFromName(status)],1));
		AutoPageColor.Common.showAlert(currentScheme);
	}),
	/**
	 * Preference listener for extension preferences change.
	**/
	prefListener : new AutoPageColor.APCPrefListener("extensions.autopagecolor.",function(branch,name){
		switch(name){
			case("enabled"): if(branch.getBoolPref("enabled",false)) {
								AutoPageColor.Common.printlog("Enabling timer.")
								AutoPageColor.Common.timer.init(AutoPageColor.Common.timeObserver,
									AutoPageColor.APC_TIMER_DELAY,AutoPageColor.Common.timer.TYPE_REPEATING_SLACK);
								if(AutoPageColor.Overlay.chkEnable){
									AutoPageColor.Overlay.chkEnable.setAttribute("checked","true");
								}
							} else {
								AutoPageColor.Common.printlog("Disabling timer.")
								AutoPageColor.Common.timer.cancel();
								if(AutoPageColor.Overlay.chkEnable){
									AutoPageColor.Overlay.chkEnable.removeAttribute("checked");
								}
							}
				break;
			case("logging"): AutoPageColor.Common.logoutput = AutoPageColor.Common.prefBranch.getBoolPref("logging",false);
				break;
		}
	}),
	cleanup : function(){
	    Services.obs.removeObserver(AutoPageColor.Common.uninstallObserver, "quit-application-granted");
    	AddonManager.removeAddonListener(AutoPageColor.Common.uninstallListener);
		AutoPageColor.Common.timer.cancel();
		AutoPageColor.Overlay.globalPrefListener.unregister();
		AutoPageColor.Overlay.prefListener.unregister();
		if(AutoPageColor.Overlay.APCTBBtn != null){
			AutoPageColor.Overlay.buttonUnload();	
		}
		window.removeEventListener("beforecustomization",AutoPageColor.Overlay.toolbarHandler.toolbarBeforeCustomization);
		window.removeEventListener("aftercustomization",AutoPageColor.Overlay.toolbarHandler.toolbarAfterCustomization);
	}
}
window.addEventListener("load",AutoPageColor.Overlay.init,false);
window.addEventListener("unload",AutoPageColor.Overlay.cleanup,false);
window.addEventListener("aftercustomization",AutoPageColor.Overlay.toolbarHandler.toolbarAfterCustomization,false);
window.addEventListener("beforecustomization",AutoPageColor.Overlay.toolbarHandler.toolbarBeforeCustomization,false);