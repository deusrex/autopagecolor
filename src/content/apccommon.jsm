var EXPORTED_SYMBOLS = ["Common","Ci","Cu","Cc","APCPrefListener","APC_TIMER_DELAY"];
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
const APC_TIMER_DELAY = 60000;
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
Cu.import("chrome://autopagecolor/content/suncalc.jsm");
Common = {
	alertTitle : null,
	darkLabel : null,
	lightLabel : null,
	noneLabel : null,
	prefBranch : Services.prefs.getBranch("extensions.autopagecolor."),
	logoutput : false,
	settingsWindow : null,
	strBundle : null,
	urlIPLookupSvc : null,
	urlGeoLookupSvc : null,
	uninstall : false,
	timer : Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer),
	alerts :Cc["@mozilla.org/alerts-service;1"].getService(Ci.nsIAlertsService),
	APCID : "{66CC6907-F773-4EB9-A01E-104221D63C25}",
    /**
     * Log output to toolkit error console if enabled.
     */ 
    printlog : function(item) {
        if(Common.logoutput == true) {
            Services.console.logStringMessage("[AutoPageColor:] "+item);
        }
    },
    formatAPCDate : function(inDate){
	    let hours = inDate.getHours() < 10 ? "0" + inDate.getHours() : inDate.getHours();
    	let mins = inDate.getMinutes() < 10 ? "0" + inDate.getMinutes() : inDate.getMinutes();
    	return hours+":"+mins;
    },
    /**
     * Performs an XmlHttpRequest to the specified URL and invokes the specified callback.
     * @param {} url
     * @param {} callback
     */
    bgRequest : function(url,callback){
		let	xhr = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"]
			.createInstance(Ci.nsIXMLHttpRequest);
		let handler = ev => {
        	evf(m => xhr.removeEventListener(m, handler, !1));
        	switch (ev.type) {
            	case 'load':
                	if (xhr.status == 200) {
                    	callback(xhr.response);
                    	break;
                	}
            	default:
                	break;
        	}    
        }
		let evf = f => ['load', 'error', 'abort'].forEach(f);
    	evf(m => xhr.addEventListener(m, handler, false));
    	xhr.mozBackgroundRequest = true;
    	xhr.open('GET', url, true);
    	xhr.channel.loadFlags |= Ci.nsIRequest.LOAD_ANONYMOUS 
    	| Ci.nsIRequest.LOAD_BYPASS_CACHE | Ci.nsIRequest.INHIBIT_PERSISTENT_CACHING;
    	xhr.send(null);    
    },
    /**
     * Notify using desktop alert if enabled. 0 = no preference, 1 = light, 2 = dark.
     */
    showAlert:function(mode){
    	let message = null;
    	let modeString = null;
    	if(mode >= 0 && mode <=2){
	    	switch(mode){
    			case 0: 
    				message = Common.strBundle.formatStringFromName("notification.text",[Common.noneLabel],1);
	    			modeString = "off";
    			break;
    			case 1:
    				message = Common.strBundle.formatStringFromName("notification.text",[Common.lightLabel],1);
	    			modeString = "day";
	    		break;
	    		case 2:
	    			message = Common.strBundle.formatStringFromName("notification.text",[Common.darkLabel],1);
		    		modeString = "night";
	    		break;
	    	}
	    	if(Common.prefBranch.getBoolPref("alerts",true)){
	    		Common.alerts.showAlertNotification("chrome://autopagecolor/skin/"+modeString+"mode.png",Common.alertTitle,message);
	    	}
    	} else {
    		message = Common.strBundle.GetStringFromName("notification.invalid");
    	}
    	Common.printlog(message);
    },
	/**
	 * Launch configuration window.
	 */
 	onConfigure : function(event){
		if(Common.settingsWindow == null || Common.settingsWindow.closed){
			let instantApply = Services.prefs.getBoolPref("browser.preferences.instantApply",true);
				let features = "chrome,titlebar,centerscreen,resize=no," +
					(instantApply.value ? "dialog=no" : "modal");
				Common.settingsWindow = Services.ww.openWindow(null,"chrome://autopagecolor/content/locationsettings.xul",
				Common.strBundle.GetStringFromName("locationsettings.title"),features,null);
		} else {
			Common.settingsWindow.focus();
		}
	},
   /**
     * Timer callback to change color scheme based on current time.
     * @type 
     */
    timeObserver : {
    	observe : function(aSubject,aTopic,aData){
    		if(aTopic == 'timer-callback' && Common.prefBranch.getBoolPref("enabled",false) 
    			&& Services.prefs.getIntPref("browser.display.prefers_color_scheme") > 0){
    			let current = new Date();
    			let sunTimes = SunCalc.getTimes(current,Common.prefBranch.getCharPref("latitude"),Common.prefBranch.getCharPref("longitude"));
    			let sunrise = new Date(sunTimes.sunrise);
    			let sunset = new Date(sunTimes.sunset);
    			let now = current.getTime();
    			Common.printlog("Inside timer at "+Common.formatAPCDate(current)+"\nsunrise at "
    				+Common.formatAPCDate(sunrise)+"\nsunset at "
    				+Common.formatAPCDate(sunset));
    			let title = Common.strBundle.GetStringFromName("notification.title");
    			let colorScheme =Services.prefs.getIntPref("browser.display.prefers_color_scheme"); 
    			if(colorScheme == 1 && (now > sunset.getTime() || now < sunrise.getTime())){
    				colorScheme = 2; //dark theme
    			} else if(colorScheme == 2 && (now > sunrise.getTime() && now < sunset.getTime())){
    				colorScheme = 1; //light theme
    			} else return;
   				Services.prefs.setIntPref("browser.display.prefers_color_scheme",colorScheme); 
    		}
    	}
    },
	/**
	 * Install toolbar button.
	 */
	installButton:function(doc,toolbarId,id,afterId ){
		if (!doc.getElementById(id)) {
			var toolbar = doc.getElementById(toolbarId);
			var before = null;
			if (afterId) {
				let elem = doc.getElementById(afterId);
				if (elem && elem.parentNode == toolbar){
					before = elem.nextElementSibling;
				}
			}
			toolbar.insertItem(id,before);
			toolbar.setAttribute("currentset", toolbar.currentSet);
			doc.persist(toolbar.id, "currentset");
			if (toolbarId == "addon-bar"){
			toolbar.collapsed = false;
				}
		}
	},
	/**
	 * Check whether extension is being uninstalled and undo if not. 
	 */
	uninstallListener :{
		onUninstalling:function(addon){
			if(addon.id.toUpperCase()==Common.APCID){
				Common.uninstall=true;
			}
		},
		onOperationCancelled: function(addon) {
    		if (addon.id.toUpperCase() == Common.APCID) {
      			Common.uninstall = (addon.pendingOperations & AddonManager.PENDING_UNINSTALL) != 0;
      		}
    	}	
	},
	/**
	 * Clean up settings when browser is restarting after a confirmed uninstall.
	 */
	uninstallObserver :{
		observe : function(aSubject,aTopic,aData){
			if(Common.uninstall == true	&& Services.prompt.confirm(Services.wm.getMostRecentWindow(null),
					Common.strBundle.GetStringFromName("notification.title"),
					Common.strBundle.GetStringFromName("uninstall.prompt"))) {
				Common.prefBranch.clearUserPref('enabled');
				Common.prefBranch.clearUserPref('firstrun');
				Common.prefBranch.clearUserPref('logging');
				Common.prefBranch.clearUserPref('alerts');
				Common.prefBranch.clearUserPref('latitude');
				Common.prefBranch.clearUserPref('longitude');
				Common.prefBranch.clearUserPref('iplookupsvc');
				Common.prefBranch.clearUserPref('geolookupsvc');
				Services.prefs.deleteBranch("extensions.autopagecolor.");
			}
		}
	}
}
function APCPrefListener(branch_name, callback) {
	// Keeping a reference to the observed preference branch or it will get
	// garbage collected.
	this._branch = Services.prefs.getBranch(branch_name);
	this._branch.QueryInterface(Ci.nsIPrefBranch2);
	this._callback = callback;
}

APCPrefListener.prototype.observe = function(subject, topic, data) {
	if (topic == 'nsPref:changed')
		this._callback(this._branch, data);
};

APCPrefListener.prototype.register = function() {
	this._branch.addObserver('', this, false);
	let that = this;
	this._branch.getChildList('', {}).
	forEach(function (pref_leaf_name){ 
		that._callback(that._branch, pref_leaf_name); 
	});
};

APCPrefListener.prototype.unregister = function() {
	if (this._branch)
		this._branch.removeObserver('', this);
};