if("undefined" == typeof(AutoPageColor)) {
	var AutoPageColor = {}
}
Components.utils.import("chrome://autopagecolor/content/apccommon.jsm", AutoPageColor);
AutoPageColor.ColorDialog = {
	rbgColorScheme : null,
	bdConfig : null,
	chkEnable : null,
	cmdConfig : null,
	cmdEnable : null,
	init : function(){
		AutoPageColor.ColorDialog.rbgColorScheme = document.getElementById("prefersColorSchemeSelection");
		AutoPageColor.ColorDialog.bdConfig = document.getElementById("bdAutoPageColConfig");
		AutoPageColor.ColorDialog.chkEnable = document.getElementById("chkAutoPageColEnable");
		AutoPageColor.ColorDialog.chkEnable.addEventListener("command",AutoPageColor.ColorDialog.onEnable,false); 
		AutoPageColor.ColorDialog.cmdConfig=document.getElementById("cmdAutoPageColConfig");
		AutoPageColor.ColorDialog.rbgColorScheme.addEventListener("command",AutoPageColor.ColorDialog.onColorScheme,false);
		AutoPageColor.ColorDialog.cmdConfig.addEventListener("command",AutoPageColor.Common.onConfigure,false);
		AutoPageColor.ColorDialog.initRadioButtons(AutoPageColor.ColorDialog.rbgColorScheme);
	},
	/**
	 * Initialize radio buttons based on settings.
	 */
	initRadioButtons : function(rbGroup){
		if(rbGroup.selectedIndex == 2){
			AutoPageColor.ColorDialog.bdConfig.setAttribute("disabled","true");
			AutoPageColor.ColorDialog.chkEnable.setAttribute("disabled","true");
			AutoPageColor.ColorDialog.chkEnable.checked = false;
		} 
		if(AutoPageColor.Common.prefBranch.getBoolPref("enabled",false)){
			AutoPageColor.ColorDialog.bdConfig.removeAttribute("disabled");
			AutoPageColor.ColorDialog.chkEnable.removeAttribute("disabled");
			AutoPageColor.ColorDialog.chkEnable.checked = true;
			rbGroup.disabled = true;
		}  else {
			AutoPageColor.ColorDialog.bdConfig.setAttribute("disabled","true");
			AutoPageColor.ColorDialog.chkEnable.checked = false;
			rbGroup.disabled = false;
		} 
	},
	/**
	 * Checkbox event handler to enable configure button only when selected. 
	 */
	onEnable : function(event){
		if(event.target.checked){
			AutoPageColor.ColorDialog.bdConfig.removeAttribute("disabled");
			AutoPageColor.ColorDialog.rbgColorScheme.disabled = true;
		}  else {
			AutoPageColor.ColorDialog.bdConfig.setAttribute("disabled","true");
			AutoPageColor.ColorDialog.rbgColorScheme.disabled = false;
		}
		AutoPageColor.Common.prefBranch.setBoolPref("enabled",event.target.checked);
	},
	/**
	 * Toggle extension automation based on whether user chooses not to
	 * specify a color scheme.
	 * @param {} event
	 */
	onColorScheme : function(event) {
		if(event.target.selectedIndex == 2){
			AutoPageColor.ColorDialog.bdConfig.disabled = true;
			AutoPageColor.ColorDialog.chkEnable.setAttribute("disabled","true");
			AutoPageColor.ColorDialog.chkEnable.checked = false;
		} else {
			AutoPageColor.ColorDialog.bdConfig.disabled = false;
			AutoPageColor.ColorDialog.chkEnable.removeAttribute("disabled");
		}
		AutoPageColor.Common.prefBranch.setBoolPref("enabled",AutoPageColor.ColorDialog.chkEnable.checked);
	},
	cleanup : function(){
		AutoPageColor.ColorDialog.cmdConfig.removeEventListener("command",AutoPageColor.Common.onConfigure);	
		AutoPageColor.ColorDialog.chkEnable.removeEventListener("command",AutoPageColor.ColorDialog.onEnable);	
		AutoPageColor.ColorDialog.rbgColorScheme.removeEventListener("command",AutoPageColor.ColorDialog.onColorScheme);
	}
}
window.addEventListener("load",AutoPageColor.ColorDialog.init,false);
window.addEventListener("unload",AutoPageColor.ColorDialog.cleanup,false);