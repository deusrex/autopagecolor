if("undefined" == typeof(AutoPageColor)) {
	var AutoPageColor = {}
}
Components.utils.import("chrome://autopagecolor/content/apccommon.jsm", AutoPageColor);
Components.utils.import("chrome://autopagecolor/content/suncalc.jsm", AutoPageColor);
Components.utils.import("resource://gre/modules/Services.jsm");
AutoPageColor.Location = {
	chkAlerts : null,
	cmdAutoDetect : null,
	externalIP : null,
	lblSunrise : null,
	lblSunset : null,
	txtLatitude : null,
	txtLongitude : null,
	init(){
		AutoPageColor.Location.chkAlerts = document.getElementById("chk-apcalerts");
		AutoPageColor.Location.cmdAutoDetect = document.getElementById("cmdAPCAutodetect");
		AutoPageColor.Location.lblSunrise = document.getElementById("lbl-apcsunrise");
		AutoPageColor.Location.lblSunset = document.getElementById("lbl-apcsunset");
		AutoPageColor.Location.txtLatitude = document.getElementById("txt-apclatitude");
		AutoPageColor.Location.txtLongitude = document.getElementById("txt-apclongitude");
		AutoPageColor.Location.cmdAutoDetect.addEventListener("command",AutoPageColor.Location.onAutoDetect,false);
		AutoPageColor.Location.prefListener.register();
		AutoPageColor.Location.lblSunrise.value = AutoPageColor.Location.formatTime(true);
		AutoPageColor.Location.lblSunset.value = AutoPageColor.Location.formatTime(false);
		AutoPageColor.Location.chkAlerts.addEventListener("command",AutoPageColor.Location.onAlerts,false);
	},
	onAutoDetect(){
		try{
		AutoPageColor.Common.bgRequest(AutoPageColor.Common.urlIPLookupSvc,function(data){
			AutoPageColor.Common.printlog("Your external IP is "+data);
			AutoPageColor.Location.externalIP = data;
			try{
				AutoPageColor.Common.bgRequest(AutoPageColor.Common.urlGeoLookupSvc+AutoPageColor.Location.externalIP,function(geo){
					geo = JSON.parse(geo);
					AutoPageColor.Common.printlog("Coordinates are "+geo.lat+" latitude "+geo.lon+" longitude.");
					AutoPageColor.Common.prefBranch.setCharPref("latitude",geo.lat);
					AutoPageColor.Common.prefBranch.setCharPref("longitude",geo.lon);
					AutoPageColor.Location.txtLatitude.value = geo.lat;
					AutoPageColor.Location.txtLongitude.value = geo.lon;
				});
			} catch(e){
				Cu.reportError("[AutoPageColor error:] Error connecting to geolocation service "+AutoPageColor.Common.urlGeoLookupSvc+"\n"+e);
				return;
			}
		});
		} catch (e) {
			Cu.reportError("[AutoPageColor error:] Error connecting to IP detection service "+AutoPageColor.Common.urlIPLookupSvc+"\n"+e);			
		}
	},
	formatTime(sunrise){
		let current = new Date();
		let sunTimes = AutoPageColor.SunCalc.getTimes(current,AutoPageColor.Common.prefBranch.getCharPref("latitude"),
			AutoPageColor.Common.prefBranch.getCharPref("longitude"));
		var datetime = new Date(sunrise ? sunTimes.sunrise : sunTimes.sunset);
		var label = AutoPageColor.Common.strBundle.formatStringFromName(
			sunrise ? "label.light" : "label.dark",[AutoPageColor.Common.formatAPCDate(datetime)],1)
		return label;
	},
	prefListener : new AutoPageColor.APCPrefListener("extensions.autopagecolor.",function(branch,name){
		switch(name){
			case("latitude"):
			case("longitude"):
				let sunTimes = AutoPageColor.SunCalc.getTimes(new Date(),branch.getCharPref("latitude"),branch.getCharPref("longitude"));
				AutoPageColor.Common.printlog("in Dialog Sunrise = "+sunTimes.sunrise+",sunset = "+sunTimes.sunset);
				AutoPageColor.Location.lblSunrise.value = AutoPageColor.Location.formatTime(true);
				AutoPageColor.Location.lblSunset.value = AutoPageColor.Location.formatTime(false);
				break;
		}
	}),
	onAlerts(event){
		let status = AutoPageColor.Common.strBundle.GetStringFromName(event.target.checked ? "notification.enabled":"notification.disabled");
		let message = AutoPageColor.Common.strBundle.formatStringFromName("notification.toggle",[status],1);
		AutoPageColor.Common.alerts.showAlertNotification("chrome://autopagecolor/skin/autopagecolor64.png",AutoPageColor.Common.alertTitle,message); 
	},
	cleanup(){
		AutoPageColor.Location.chkAlerts.removeEventListener("command",AutoPageColor.Location.onAlerts);
		AutoPageColor.Location.prefListener.unregister();
		AutoPageColor.Location.cmdAutoDetect.removeEventListener("command",AutoPageColor.Location.onAutoDetect);
	}
}
window.addEventListener("load",AutoPageColor.Location.init,false);
window.addEventListener("unload",AutoPageColor.Location.cleanup,false);